### Hi there 👋

- 📫 You can reach me at Telegram [@Kotlerdev](https://t.me/kotlerdev).

### Java Developer  
- Java: Java Core, Spring, Spring Boot, Lombok, Maven.  
- Kotlin: Kotlin Core, Jetpack Compose.  
- Database: JDBC, SQL, PostgreSQL, H2, Hibernate, JPA.  
- Web: HTML, CSS, JS, Selenium, Figma, Miro.  
- CVS: Git CLI, Gitlab, Github, Confluence.  
- Pattern: MVC, CRUD.  
- Linux: Fedora Gnome DE, Archlinux, Debian.  
- Management: Jira, Trello.  
